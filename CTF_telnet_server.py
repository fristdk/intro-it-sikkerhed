#!/usr/bin/python3

import socket

def main():
    # Definerer host og port
    host = '0.0.0.0'
    port = 42023

    # Opretter en socket
    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    
    try:
        # Binder socketen til host og port
        server_socket.bind((host, port))
        
        # Lytter efter forbindelser
        server_socket.listen(5)
        
        print("Telnet server er startet på {}:{}".format(host, port))
        
        while True:
            # Accepterer en ny forbindelse
            client_socket, client_address = server_socket.accept()
            
            print("Forbindelse accepteret fra:", client_address)
            
            # Sender en besked til klienten
            message = "Velkommen til Telnet server!\r\n"
            client_socket.send(message.encode('utf-8'))
            
            # Lukker forbindelsen til klienten
            client_socket.close()
            print("Forbindelse lukket med:", client_address)
            
    except Exception as e:
        print("Der opstod en fejl:", e)
    finally:
        # Sørg for at lukke server socketen i tilfælde af en fejl
        server_socket.close()

if __name__ == "__main__":
    main()