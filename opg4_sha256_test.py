#! /usr/bin/python3

import hashlib

while True:
    pw = input("skriv dit forsøg her, hvis færdig skriv 'done': ")
    if pw == "done":
        print('tak for i dag og vend snart tilbage')
        break
    h = hashlib.sha256()
    h.update(pw.encode('utf-8'))
    hex = h.hexdigest()
    print(hex)
